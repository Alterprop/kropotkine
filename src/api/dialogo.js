export const Alerta = (titulo, mensaje, botones) =>
  navigator.notification.alert(mensaje, null, titulo, botones[0]);

export const Confirmacion = (titulo, mensaje, botones) => {
  return new Promise((resolve, reject) => {
    navigator.notification.confirm(
      mensaje,
      function(buttonIndex) {
        if (buttonIndex === 1) {
          return resolve(buttonIndex);
        } else {
          return reject(buttonIndex);
        }
      },
      titulo,
      [botones[0], botones[1]]
    );
  });
};

export const Prompt = (titulo, mensaje, botones) => {
  return new Promise((resolve, reject) => {
    navigator.notification.prompt(
      mensaje,
      function(res) {
        if (res.buttonIndex === 1) {
          return resolve(res.input1);
        } else {
          return reject(res.buttonIndex);
        }
      },
      titulo,
      [botones[0], botones[1]],
      ''
    );
  });
};

export const Aviso = (titulo, duracion, boton, cb) => {
  //cordova.plugins.snackbar(titulo, duracion, boton, cb);
};

export const Spinner = {
  //  start: () => (SpinnerPlugin.activityStart("Cargando...")),
  //  stop: () => (SpinnerPlugin.activityStop())
};

const proto = {
  alerta: Alerta,
  confirmacion: Confirmacion,
  prompt: Prompt,
  aviso: Aviso,
  spinner: Spinner
};

export default Object.create(proto);
