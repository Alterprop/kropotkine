import PouchDB from 'pouchdb';
import PouchDBFindPlugin from 'pouchdb-find';
import { ObjectID } from 'bson';
import { CryptoFunc } from '../lib/xml2Json';

PouchDB.plugin(PouchDBFindPlugin);
PouchDB.debug.enable('pouchdb:find');

const localDB = new PouchDB('kropotkine_db');
const remoteDB = new PouchDB('http://localhost:5984/kropotkine_db');

const query = (key) => {
  console.log(key);
  function myMapFunction(doc) {
    PouchDB.emit(CryptoFunc.decrypt(doc.libel, key));
  }

  return localDB.query(myMapFunction, {
    include_docs : true
  }).then(function (result) {
    console.log(result);
  }).catch(function (err) {
    console.log(err);
  });
};

const insert = doc => {
  let _id = new ObjectID();

  return localDB.put(
    Object.assign({}, doc, {
      ediciones: [],
      _id: _id.toString()
    })
  );
};

const update = updt => {
  if (Array.isArray(updt._id)) return localDB.bulkDocs(updt);

  return localDB.get(updt._id).then(doc => {
    doc.ediciones.push(Date.now());
    localDB.put(Object.assign({}, doc, updt));
  });
};

const findOne = (_id, opts) => {
  if (!opts)
    opts = {
      attachments: true
    };

  return localDB.get(_id, opts);
};

const find = ext => {
  let opts = {
    include_docs: true
  };

  return localDB.allDocs(Object.assign({}, opts, {}));
};

const remove = _id =>
  localDB
    .get(_id)
    .then(doc => localDB.remove(doc))
    .catch(err => console.log(err));

const sync = opciones => {
  if (!opciones)
    opciones = {
      live: true,
      retry: false
    };

  return localDB.sync(remoteDB, opciones);
};

const syncEvs = () =>
  sync()
    .on('change', change => change)
    .on('paused', info => info)
    .on('active', info => info)
    .on('complete', info => info)
    .on('error', err => err);

const unsync = () => sync.cancel();

const init = () => localDB.info(); //sync())

export default {
  init: init,
  insert: insert,
  update: update,
  find: find,
  findOne: findOne,
  remove: remove,
  sync: sync,
  syncEvs: syncEvs,
  unsync: unsync,
  query: query
};
