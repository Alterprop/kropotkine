import $http from './fetch.js';
import { CryptoFunc } from '../lib/xml2json';
import { parse, jsxify } from '../lib/himalaya';

export const Authors = [
  { name: 'Henry Kissinger' },
  { name: 'Margaret Thatcher' },
  { name: 'Ronald Reagan' },
  { name: 'Hillary Clinton' },
  { name: 'Donald Trump' }
];

export const getRandomAuthor = () => {
  return Authors[Math.floor(Math.random() * Authors.length)];
};

export const wikiSearch = options => {
  const objProps = Object.keys(options);
  let requestParams = objProps.reduce((acc, prop, index) => {
    let nexo = index < objProps.length - 2 ? '&' : '';
    if (prop !== 'lang') {
      acc = `${acc}${prop}=${encodeURI(options[prop])}${nexo}`;
    }
    return acc;
  }, '');

  return `https://${options.lang ||
    'en'}.wikiquote.org/w/api.php?${requestParams}`;
};

export const byAuthor = (author, lang) => {

  const url = wikiSearch({
    format: 'json',
    action: 'query',
    titles: author,
    origin: '*',
    lang: lang || 'en'
  });

  return $http({
    method: 'GET',
    url,
    headers: { 'Api-User-Agent': 'Example/1.0' }
  }).then(
    res => (res && res.query ? Object.keys(res.query.pages)[0] : 8563), // Default Henry Kissinger
    err => console.log(err)
  );
};

export const sectionSearch = (pageid, lang) => {
  if (!pageid) return null;

  const url = wikiSearch({
    format: 'json',
    action: 'parse',
    pageid,
    prop: 'sections',
    origin: '*',
    lang
  });

  return $http({
    method: 'GET',
    url,
    headers: { 'Api-User-Agent': 'Example/1.0' }
  }).then(
    res => {

      if (res.error) throw res.error;

      let sections = res.parse.sections
        .filter(
          section =>
            section.number.split('.').length > 1 &&
            section.number.split('.')[0] === '1'
        )
        .map(section => section.index);

      if (!sections.length) {
        sections = [].concat(sections, [1]);
      }

      return { pageid, sections };
    },
    err => console.log(err)
  );
};

export const calculatePath = image => {
  const hash = `${CryptoFunc.md5(image).charAt(0)}/${CryptoFunc.md5(image).substr(
    0,
    2
  )}`;
  return `https://upload.wikimedia.org/wikipedia/commons/${hash}/${image}`;
};

export const quoteSearch = ({ pageid, sections, lang }) => {

  let section = Math.floor(Math.random() * sections.length);

  if (section <= 0) {
    section = 1;
  }

  const url = wikiSearch({
    format: 'json',
    action: 'parse',
    pageid,
    noimages: '',
    section: section,
    origin: '*',
    lang
  });

  return $http({
    method: 'GET',
    url,
    headers: { 'Api-User-Agent': 'Example/1.0' }
  }).then(
    res => {

      const parsedContent = document.createElement('p');
      parsedContent.innerHTML = res.parse.text['*'];

      let quotes = [].slice
        .call(parsedContent.childNodes[0].childNodes)
        .filter(node => node.tagName === 'UL');

      const randomQuote = quotes[Math.floor(Math.random() * quotes.length)];

      // Remove quote details;
      const uls = randomQuote.childNodes[0].getElementsByTagName('ul');
      while (uls[0]) randomQuote.childNodes[0].removeChild(uls[0]);

      const quote = randomQuote.childNodes[0];
      return {
        quote,
        libel:  jsxify(parse(quote.textContent)),
        image: res.parse.images.length
          ? calculatePath(res.parse.images[0])
          : null
      };
    }
  ).catch(err => console.log(err));
};

export const getQuote = (author, lang) =>
  byAuthor(author, lang)
    .then(response => sectionSearch(response, lang))
    .then(response => quoteSearch(Object.assign({}, response, { lang }) ) )
    .catch(error => ({ error }));

const WikiQuoteApi = () => ({
  wikiSearch,
  byAuthor,
  getQuote,
  calculatePath,
  getRandomAuthor,
  sectionSearch
});

const Wiki = WikiQuoteApi();

export default Wiki;
