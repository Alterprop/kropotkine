import Almacen from './almacen.js';
import CryptoJS from 'crypto-js';

const Crypto = () => ({

          encripta : (cosa) => {
               if (cosa && typeof cosa === 'string' && Almacen.getPin()) {
                   return CryptoJS.AES.encrypt(cosa, Almacen.getPin().codigo).toString();
               }
          },

          desencripta : (cosa) => {
               if (cosa && Almacen.getPin()) {
                   try {
                    return  CryptoJS.AES.decrypt(cosa, Almacen.getPin().codigo).toString(CryptoJS.enc.Utf8)
                   } catch (e) {
                    return ""
                   }
               } else {
                   return "";
               }
          },

          md5 : (cosa) => {

              return CryptoJS.MD5(cosa).toString()

          }

});

export default Crypto();
