//import React from 'react';
import db from '../lib/db';
import { toJson, toHtml } from '../lib/xml2json';

export const insert = (note, key) => {
  return db.insert({
    libel: toJson(note, key),
    time: Date.now()
  });
};

export const update = (note, key) => {
  return db.update(
    Object.assign({}, note, {
      libel: toJson(note.libel, key)
    })
  );
};

export const remove = id => {
  return db.remove(id);
};

export const find = (key, persistent) => {

  if (persistent) {
    setKey(key);
  }

  return db.find().then(
    res => {
      return res
        ? res.rows.map(note => jsxify(note.doc, key)).filter(doc => doc.libel)
        : [];
    },
    err => console.log(err)
  );
};

export const getKey = () => JSON.parse(localStorage.getItem('fakebook')) || '';

export const setKey = key => {
  if (getKey() === key) return;
  localStorage.setItem(
    'fakebook',
    JSON.stringify(key)
  );
};

export const findOne = (id, key) => {
  return db.findOne(id).then(doc => jsxify(doc, key));
};

function jsxify(doc, key) {
  return Object.assign({}, doc, { libel: toHtml(doc.libel, key) });
}

export default {
  insert: insert,
  remove: remove,
  findOne: findOne,
  update: update,
  find: find,
  getKey: getKey,
  setKey: setKey
};
