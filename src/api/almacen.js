const Almacen = () => ({
  getPin: () => JSON.parse(localStorage.getItem('pin')) || '',

  setPin: codigo => {
    console.log('SET', codigo);
    localStorage.setItem(
      'pin',
      JSON.stringify({ codigo: codigo, tiempo: Date.now() })
    );
  },

  estaAbierto: () => localStorage.getItem('pin')
});

export default Almacen();
