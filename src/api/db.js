// BASE DE DATOS - MÉTODOS EQUIVALENTES A MONGO

const indexedDB =
  window.indexedDB ||
  window.mozIndexedDB ||
  window.webkitIndexedDB ||
  window.msIndexedDB ||
  window.shimIndexedDB;

const IDB_NOMBRE = 'kropotkine';
const IDB_VERSION = '1';
const IDB_STORE = 'panfletos';

export const Base = () => ({ db: null });

const init = () => {
  return new Promise((resolve, reject) => {
    let open = indexedDB.open(IDB_NOMBRE, IDB_VERSION);

    open.onsuccess = e => {
      Base.db = e.target.result;
      resolve('[Base]: abierta');
    };

    open.onerror = e => {
      reject('[Base]: error');
    };

    open.onupgradeneeded = e => {
      Base.db = e.target.result;
      let store = Base.db.createObjectStore(IDB_STORE, {
        keyPath: '_id',
        autoIncrement: true
      });
      store.createIndex('panfleto', 'panfleto', { unique: false });
      store.createIndex('tiempo', 'tiempo', { unique: false });
      let transaction = e.target.transaction;
      transaction.oncomplete = ev => resolve('[Base]: onupgradeneeded');
    };
  });
};

const _transaccion = () => {
  return Base.db.transaction(IDB_STORE, 'readwrite').objectStore(IDB_STORE);
};

const drop = () => indexedDB.deleteDatabase(nombre);

const insert = obj => {
  obj.ediciones = [];
  return new Promise((resolve, reject) => {
    var request = _transaccion().add(obj);

    request.onsuccess = e => {
      resolve(e.target.result);
    };
    request.onerror = e => reject(e.target.errorCode);
  });
};

const find = () => {
  return new Promise((resolve, reject) => {
    let panfletos = [];

    let request = _transaccion().openCursor();

    request.onsuccess = function(e) {
      var cursor = e.target.result;
      if (cursor) {
        panfletos.push(cursor.value);
        cursor.continue();
      } else {
        resolve(panfletos);
      }
    };

    request.onerror = err => reject(err);
  });
};

const findOne = id => {
  return new Promise((resolve, reject) => {
    var request = _transaccion().get(Number(id));

    request.onsuccess = e => {
      let resultado = e.target.result;

      resolve(resultado);
    };
    request.onerror = e => reject(e.target.errorCode);
  });
};

const update = obj => {
  return new Promise((resolve, reject) => {
    if (!obj.panfleto.length) return reject('No hay panfleto');

    const request = _transaccion().openCursor();
    request.onsuccess = function(e) {
      let cursor = e.target.result;
      if (cursor) {
        if (cursor.value._id === obj._id) {
          for (let prop in obj) {
            if (cursor.value.hasOwnProperty(prop)) {
              if (obj[prop] !== cursor.value[prop]) {
                cursor.value[prop] = obj[prop];
              }
            } else {
              cursor.value[prop] = obj[prop];
            }
          }
          cursor.value.ediciones.push(Date.now());
          const actualizacion = cursor.update(cursor.value);
          actualizacion.onsuccess = ev => {
            resolve(ev.target.result);
          };
          actualizacion.onerror = ev => {
            reject(ev);
          };
        } else {
          cursor.continue();
        }
      }
    };

    request.onerror = err => reject(err);
  });
};

const del = _id => {
  return new Promise((resolve, reject) => {
    var request = _transaccion().delete(_id);
    request.onsuccess = e => resolve(e.target.result);
    request.onerror = e => reject(e.target.errorCode);
  });
};

export default {
  Base: Base,
  init: init,
  drop: drop,
  insert: insert,
  update: update,
  find: find,
  findOne: findOne,
  remove: del
};
