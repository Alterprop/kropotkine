import React from 'react';
import cryptoJS from 'crypto-js';
import { parse, jsxify } from './himalaya';

export const CryptoFunc = {
  encrypt: (cosa, key) => {
    if (cosa && typeof cosa === 'string') {
      return cryptoJS.AES.encrypt(cosa, key).toString();
    }
  },

  decrypt: (cosa, key) => {
    if (cosa) {
      try {
        return cryptoJS.AES.decrypt(cosa, key).toString(cryptoJS.enc.Utf8);
      } catch (error) {
        return null;
      }
    } else {
      return '';
    }
  },

  md5: cosa => {
    return cryptoJS.MD5(cosa).toString();
  }
};

export const toHtml = (libel, key) => {
  const decryptedLibel = CryptoFunc.decrypt( libel, key );
  if (decryptedLibel) {
    return jsxify(
      JSON.parse(
        decryptedLibel
      )
    );
  } else {
    return null;
  }
};

export const toJson = (libel, key) => {
  return libel ? CryptoFunc.encrypt(
    JSON.stringify(
      parse(libel.innerHTML)
    ), key
  ) : '';
};

// http://davidwalsh.name/convert-xml-json
export default function xmlToJson(xml) {
  let obj = {};

  if (xml.nodeType === 1) {
    /*if (xml.attributes.length) {
      [].slice.call(xml.attributes)
        .forEach(
          (attr) => obj[attr.nodeName] = attr.nodeValue
        );
    }*/
  } else if (xml.nodeType === 3) {
    obj = xml.nodeValue;
  }

  if (xml.hasChildNodes()) {
    if (xml.childNodes.length === 1 && xml.childNodes[0].nodeType === 3) {
      obj = xml.childNodes[0].nodeValue;
    } else {
      [].slice.call(xml.childNodes).forEach(node => {
        const nodeName = node.nodeName;
        if (typeof obj[nodeName] === 'undefined') {
          obj[nodeName] = xmlToJson(node);
        } else {
          if (!Array.isArray(obj[nodeName])) {
            obj[nodeName] = [].concat([obj[nodeName]]);
          }
          obj[nodeName] = [].concat(obj[nodeName], xmlToJson(node));
        }
      });
    }
  }

  return obj;
}
