const proto = {
  deconstruction: (nota) => console.log(nota),
  construction: (nota) => console.log(nota)
};

export default Object.create(proto);
