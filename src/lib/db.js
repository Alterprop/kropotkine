import PouchDB from 'pouchdb';
import { ObjectID } from 'bson';
import Error from './errorCodes.js';

/*
const ObjectID = (
  m = Math,
  d = Date,
  h = 16,
  s = s => m.floor(s).toString(h)
) => s(d.now() / 1000) + ' '.repeat(h).replace(/./g, () => s(m.random() * h));
*/

const localDB = new PouchDB('kropotkine@', {revs_limit: 1, auto_compaction: true});
const remoteDB = new PouchDB('http://localhost:5984/kropotkine@');

const insert = doc => {
  const _id = new ObjectID();

  return localDB.put(
    Object.assign({}, doc, {
      _id: _id.toString(),
      editions: []
    })
  );
};

const update = note =>
  localDB.get(note._id).then(doc =>
    localDB.put(
      Object.assign({}, doc, note, {
        editions: [].concat(doc.editions, [Date.now()])
      })
    )
  );

const findOne = id => localDB.get(id);

const find = () =>
  localDB
    .allDocs({
      include_docs: true,
      descending: true
    })
    .then(
      docs => Object.assign({}, docs, { errorCode: 100 }),
      error => {
        throw Object.assign({}, Error[102], {
          error: error
        });
      }
    );

const remove = _id =>
  localDB.get(_id).then(
    doc => localDB.remove(doc),
    error => {
      throw Object.assign({}, Error[105], {
        error: error
      });
    }
  );

const defaultSyncOptions = {
  live: true,
  retry: false
};

const sync = (options = defaultSyncOptions) => localDB.sync(remoteDB, options);

const syncEvs = () =>
  sync()
    .on('change', change => change)
    .on('paused', info => info)
    .on('active', info => info)
    .on('complete', info => info)
    .on('error', err => err);

const unsync = () => sync.cancel();

const init = () => localDB.info(); //sync())

export default {
  init: init,
  insert: insert,
  update: update,
  find: find,
  findOne: findOne,
  remove: remove,
  sync: sync,
  syncEvs: syncEvs,
  unsync: unsync
};
