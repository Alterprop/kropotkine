const initialState = {
  key: null
};

export default (state = initialState, payload) => {
  switch (payload.type) {
  case 'BOOK_CREATE':
    return Object.assign({}, state, { key: payload.key });
  default:
    return state;
  }
};
