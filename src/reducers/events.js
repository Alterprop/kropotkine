const initialState = {
  scrollUp: false,
  scrollDown: false,
  scrollX: false,
  scrollY: false,
  editorY: false
};

export default (state = initialState, payload) => {
  switch (payload.type) {
  case 'SCROLL_UP':
    return Object.assign({}, state, { scrollUp: payload.up });
  case 'SCROLL_DOWN':
    return Object.assign({}, state, { scrollDown: payload.down });
  case 'SCROLL_VERTICAL':
    return Object.assign({}, state, { scrollY: payload.vertical });
  case 'EDITOR_VERTICAL':
    return Object.assign({}, state, { editorY: payload.vertical });
  default:
    return state;
  }
};
