import api from '../api/api';

const NOTES_FIND = 'NOTES_FIND';
const NOTES_FINDONE = 'NOTES_FINDONE';
const NOTES_INSERT = 'NOTES_INSERT';
const NOTES_UPDATE = 'NOTES_UPDATE';
const NOTES_REMOVE = 'NOTES_REMOVE';
const NOTES_REMOVE_CURRENT = 'NOTES_REMOVE_CURRENT';

export const find = notes => ({
  type: NOTES_FIND,
  notes
});

export const fetch = key => {
  return dispatch => {
    return api
      .find(key)
      .then(res => dispatch(find(res)), error => console.log(error));
  };
};

export const insertNote = note => ({
  type: NOTES_INSERT,
  note
});

export const insert = (note, key) => {
  console.log(note, key);
  return dispatch => {
    return api
      .insert(note, key)
      .then(res => dispatch(insertNote(res, key)))
      .then(() => dispatch(fetch(key)))
      .catch(err => console.log(err));
  };
};

export const updateNote = note => ({
  type: NOTES_UPDATE,
  note
});

export const update = (note, key) => {
  return dispatch => {
    return api
      .update(note, key)
      .then(res => dispatch(updateNote(res)), error => console.log(error))
      .then(() => dispatch(fetch(key)), error => console.log(error));
  };
};

export const removeNote = id => ({
  type: NOTES_REMOVE,
  id
});

export const remove = id => {
  return dispatch => {
    return api
      .remove(id)
      .then(() => dispatch(removeNote(id)), error => console.log(error));
  };
};

export const findOne = note => ({
  type: NOTES_FINDONE,
  note
});

export const fetchOne = (id, key) => {
  return dispatch => {
    return api
      .findOne(id, key)
      .then(res => dispatch(findOne(res)), error => console.log(error));
  };
};

export const removeCurrent = () => ({
  type: NOTES_REMOVE_CURRENT,
  note: { time: Date.now() }
});

export default {
  insert,
  update,
  remove,
  findOne: fetchOne,
  find: fetch,
  fetch,
  removeCurrent
};
