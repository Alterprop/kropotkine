const SCROLL_UP = 'SCROLL_UP';
const SCROLL_DOWN = 'SCROLL_DOWN';
const SCROLL_VERTICAL = 'SCROLL_VERTICAL';
const EDITOR_VERTICAL = 'EDITOR_VERTICAL';

export const scrollUp = up => ({
  type: SCROLL_UP,
  up
});

export const scrollDown = down => ({
  type: SCROLL_DOWN,
  down
});

export const scrollY = vertical => ({
  type: SCROLL_VERTICAL,
  vertical
});

export const editorY = vertical => ({
  type: EDITOR_VERTICAL,
  vertical
});

export default {
  scrollUp,
  scrollDown,
  scrollY,
  editorY,
};
