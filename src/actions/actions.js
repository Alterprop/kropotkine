import bookActions from './books';
import noteActions from './notes';
import eventActions from './events';


export default Object.assign(
  {},
  bookActions,
  noteActions,
  eventActions
);
