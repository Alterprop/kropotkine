import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { isValid } from '../../api/utils';
import {
  ButtonBar,
  Page,
  Toolbar,
  Editor
} from '../../components';
import actions from '../../actions/actions';

const initialState = {
  isEditing: false
};

class Note extends Component {

  constructor(props) {
    super(props);
    this.Editor = React.createRef();
    this.state = initialState;
    this.saveLibel = this._save.bind(this);
    this._shouldSave = this._shouldSave.bind(this);
    this._cancel = this._cancel.bind(this);
    this._remove = this._remove.bind(this);
    this._key = this.props.books.key;
  }

  componentDidMount() {
    if (!this._key) this.props.history.push('/');
    const { id } = this.props.match.params;
    const newNote = id === 'new';
    if (isValid(id)) {
      this.setState({ isEditing: id }, () => this.props.findOne(id, this._key));
    } else if (newNote) {
      this.props.removeCurrent();
    }
  }

  componentWillReceiveProps(nextProps) {
    const { id } = nextProps.match.params;
    const isNewId = isValid(id) && id !== this.props.match.params.id;
    const newNote =
      !isNewId && id === 'new' && id !== this.props.match.params.id;
    if (isNewId) {
      this.props.findOne(id, this._key);
    } else if (newNote) {
      this.props.removeCurrent();
    }
  }

  _save() {
    const { id } = this.props.match.params;
    const { _editor } = this.Editor.current;
    if (isValid(id) && this._shouldSave()) {
      this.props
        .update({ _id: id, libel: _editor.current }, this._key)
        .then(res => this._cancel(res));
    } else if (isValid(id) && !this._shouldSave()) {
      this._remove();
    } else if (id === 'new' && this._shouldSave()) {
      this.props
        .insert(_editor.current, this._key)
        .then(res => this._cancel(res))
        .catch(err => console.log(err));
    } else {
      this._cancel();
    }
  }

  _shouldSave() {
    const { _editor } = this.Editor.current;
    const { childNodes } = _editor.current;
    return (
      childNodes.length &&
      !(childNodes.length === 1 && childNodes[0].nodeName === 'BR')
    );
  }

  _remove() {
    const { id } = this.props.match.params;
    if (isValid(id)) {
      this.props.remove(id).then(res => this._cancel(res));
    }
  }

  _cancel(goHome) {
    const { history } = this.props;
    return goHome ? history.push('/') : history.goBack();
  }

  render() {
    const { current } = this.props.notes;
    const { editorY } = this.props.events;

    const topButtons = [
      { func: this._cancel, value: 'Cancel', icon: 'cancel' }
    ];

    const bottomButtons = [
      {
        func: this._remove,
        icon: 'remove',
        confirm: true
      }
    ];

    return (
      <Page component="main" transitionName="example">

        <Toolbar
          time={current.time}
          backFunc={this.saveLibel}
          backIcon="back"
          actionItems={topButtons}
          actionClasses= { editorY ? 'is-scrolling' : '' }
          hasBackButton
        />

        <Editor ref={this.Editor } {...this.props} />

        <div layout="row">
          {this.state.isEditing ? <ButtonBar items={bottomButtons} /> : null}
        </div>

      </Page>
    );
  }
}

Note.defaultProps = {
  match: null,
  history: null,
  _id: null
};

Note.propTypes = {
  _id: PropTypes.string,
  match: PropTypes.object,
  history: PropTypes.object,
  findOne: PropTypes.func,
  insert: PropTypes.func,
  remove: PropTypes.func,
  removeCurrent: PropTypes.func,
  update: PropTypes.func,
  notes: PropTypes.shape({
    current: PropTypes.shape({
      _id: PropTypes.string,
      libel: PropTypes.string,
      _rev: PropTypes.string
    })
  }),
  books: PropTypes.shape({
    key: PropTypes.string
  }),
  events: PropTypes.shape({
    editorY: PropTypes.bool
  })
};

export default connect(
  state => state,
  dispatch => bindActionCreators(actions, dispatch)
)(Note);
