import React from 'react';
import PropTypes from 'prop-types';
import { Quote } from '../../views';

export default function Quotes(props) {
  return (
    <Quote {...props} />
  );
}

Quotes.defaultProps = {};

Quotes.propTypes = {
  id: PropTypes.string
};
