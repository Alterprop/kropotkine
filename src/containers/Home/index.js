import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';

import Notes from '../Notes';
import Note from '../Note';
import Quote from '../Quotes';

export default function Home() {
  return (
    <div className="page">
      {/* <Profile avatar avatarSize="80"/> */ }
      <Route path="/" component={ Notes } />
      <Route exact path="/quote" component={ Quote } />
      <Route path="/notes/:id" component={ Note } />
    </div>
  );
}

Home.defaultProps = {
};

Home.propTypes = {
  id: PropTypes.string
};
