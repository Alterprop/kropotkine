import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../../actions/actions';

import { getQuote, getRandomAuthor } from '../../api/wiki';
import { Page, Empty, Loader, Toolbar, Input } from '../../components';

const initialState = {
  lang: 'en',
  currentAuthor: null,
  avatar: '/static/img/icon.png'
};

class Quote extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
    this._searchByAuthor = this._searchByAuthor.bind(this);
    this._save = this._saveQuote.bind(this);
    this._cancel = this._cancel.bind(this);
    this._onChange = this._onChange.bind(this);
  }

  componentDidMount() {
    this._searchByAuthor();
  }

  _searchByAuthor() {
    this.setState({ isLoading: true });
    if (!this.state.currentAuthor) {
      const { name } = getRandomAuthor();
      return this.setState({ currentAuthor: name }, () => this._getQuote());
    }
    this._getQuote();
  }

  _getQuote() {
    getQuote(this.state.currentAuthor, 'en').then(res => {
      const { libel, quote, image, error } = res;
      const avatar = image ? image : initialState.avatar;
      this.setState({ libel, quote, avatar, error, isLoading: false });
    });
  }

  _saveQuote() {
    if (!this.state.quote) return this._cancel();
    this.props
      .createBook()
      .then(res => {
        return this.props.insert(this.state.quote, res.key);
      })
      .then(res => this._cancel(res))
      .catch(err => console.log(err));
  }

  _cancel() {
    const { history } = this.props;
    history.push('/');
  }

  _onChange(event) {
    const currentAuthor = event.target.value
      .split(' ')
      .map(
        word => `${word.charAt(0).toUpperCase()}${word.slice(1).toLowerCase()}`
      )
      .join(' ');

    this.setState({ currentAuthor });
  }

  render() {
    const topButtons = [
      { func: this._cancel, value: 'Cancel', icon: 'cancel' }
    ];

    return (
      <Page component="main" className="quote" transitionName="example">
        <Toolbar
          actionItems={topButtons}
          backFunc={this._save}
          backIcon="back"
          hasBackButton
        />
        <div className="quote-container">
          <figure
            className="avatar"
            style={{
              backgroundImage: `url(${this.state.avatar})`,
              filter: 'sepia(58%)'
            }}
          />

          <Input
            name="author"
            label="Autor"
            onChange={this._onChange}
            value={this.state.currentAuthor}
            onBlur={this._searchByAuthor}
          />

          <p className="quote-content">
            {!this.state.isLoading && this.state.libel
              ? this.state.libel
              : null}
            {!this.state.isLoading && this.state.error ? <Empty /> : null}
            {this.state.isLoading ? <Loader /> : null}
          </p>
        </div>
      </Page>
    );
  }
}

Quote.defaultProps = {};

Quote.propTypes = {
  children: PropTypes.element,
  insert: PropTypes.func,
  createBook: PropTypes.func,
  history: PropTypes.object,
};

export default connect(
  state => state,
  dispatch => bindActionCreators(actions, dispatch)
)(Quote);
