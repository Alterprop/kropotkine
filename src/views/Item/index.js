import React from 'react';
import PropTypes from 'prop-types';

export default function Item(props) {
  return (
    <li>
      Item  {props.item._id}
    </li>
  );
}

Item.defaultProps = {
};

Item.propTypes = {
  item: PropTypes.shape({
    _id: PropTypes.string
  })
};
