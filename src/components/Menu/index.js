import React, { Component, Children, cloneElement } from 'react';
import PropTypes from 'prop-types';
import ButtonBar from '../ButtonBar';
import { IconButton } from '../Button';
import ActionPanel from '../Page';

const initialState = {
  openMenu: false
};

export class Dropdown extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
    this._menuCancelFunc = this._menuCancelFunc.bind(this);
    this._menuToggleFunc = this._menuToggleFunc.bind(this);
  }

  componentDidMount() {
    document.body.addEventListener('click', this._menuCancelFunc);
  }

  componentDidUnmount() {
    document.body.removeEventListener('click', this._menuCancelFunc);
  }

  _menuCancelFunc(event) {
    if (this.state.openMenu && event.target.className !== 'menu-item') {
      this.setState({ openMenu: false });
    }
  }

  _menuToggleFunc(event) {
    if (this.state.openMenu) {
      console.log(event.target);
      return this._menuCancelFunc(event) ;
    }
    this.setState({ openMenu: true });
  }

  render() {
    const { children } = this.props;
    const ClickableChild = Children.map(children, (child, index) => {
      if (index === 0) {
        return cloneElement(child, { onClick: this._menuToggleFunc });
      }
      return (
        <DropdownMenu openMenu={this.state.openMenu}>{child}</DropdownMenu>
      );
    });
    return <span>{ClickableChild}</span>;
  }
}

export default function Menu(props) {
  const { items, transition, children } = props;
  return (
    <ActionPanel transitionName={transition}>
      <menu className="dropdown">
        {items ? <ButtonBar items={items} /> : null}
        {children}
      </menu>
    </ActionPanel>
  );
}

export function DropdownMenu(props) {
  const { menuCancelFunc, openMenu, icon } = props;
  return (
    <span>
      {icon ? <IconButton func={menuCancelFunc} icon={icon} /> : null}
      {openMenu ? <Menu {...props} /> : null}
    </span>
  );
}

DropdownMenu.defaultProps = Menu.defaultProps = {
  transition: 'example'
};

DropdownMenu.propTypes = Menu.propTypes = {
  title: PropTypes.string,
  transition: PropTypes.string,
  menuCancelFunc: PropTypes.func,
  items: PropTypes.arrayOf(
    PropTypes.shape({
      path: PropTypes.string.isRequired,
      alias: PropTypes.string
    })
  ),
  actionItems: PropTypes.arrayOf(
    PropTypes.shape({
      func: PropTypes.func.isRequired,
      value: PropTypes.string
    })
  )
};
