import React from 'react';
import PropTypes from 'prop-types';
import Icon from '../Icon';

export default function Loader(props) {
  return (
    <section className="loader">
      <Icon shape={props.icon} />
      <h3>{props.text}</h3>
    </section>
  );
}

Loader.defaultProps = {
  icon: 'new',
  text: 'Loading...'
};

Loader.propTypes = {
  icon: PropTypes.string,
  text: PropTypes.string
};
