import React, { Component } from 'react';

export function withScroll(WrappedComponent) {
  return class ReturnedComp extends Component {
    constructor(props) {
      super(props);
      this.state = {
        scrollPosition: 0
      };
      this._onScroll = this._onScroll.bind(this);
      console.log(this.props);
    }

    componentDidMount() {
      //this.props.scrollDown(false);
      //this.props.scrollY(false);
    }

    componentWillUnmount() {}

    _onScroll(event) {
      const {
        offsetHeight,
        scrollHeight,
        offsetTop,
        scrollTop,
        className
      } = event.nativeEvent.target;

      const top = scrollTop - offsetTop;

      if (className === 'list') {
        this.props.scrollY(
          scrollTop !== 0 && scrollTop !== scrollHeight - offsetHeight
        );
        this.props.scrollDown(top > this.state.scrollPosition);
      } else {
        this.props.editorY(
          scrollTop !== 0 && scrollTop !== scrollHeight - offsetHeight
        );
      }

      console.log('EVENT');

      this.setState({
        scrollPosition: top
      });
    }

    render() {
      return <WrappedComponent scrollFn={this._onScroll} {...this.props} />;
    }
  };
}
