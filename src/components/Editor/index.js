import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { objProp } from '../../api/utils';
import { commandList, getX } from '../../api/editor';
import EditorActions from '../EditorActions';

const commands = commandList;
const initialState = {
  textStyle: objProp(commands, false),
  isEditing: false,
  showActionBar: false
};

class Editor extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
    this._editor = React.createRef();
    this._select = this._select.bind(this);
    this._textStyle = this._textStyle.bind(this);
  }

  componentDidMount() {}

  _select(event) {
    const selection = window.getSelection();
    console.log('Select');

    const commandState = commands.reduce((acc, command) => {
      return Object.assign(
        {},
        acc,
        objProp(command, document.queryCommandState(command))
      );
    }, {});

    const position = {
      x: getX(event),
      y: event.nativeEvent.pageY - 72
    };

    this.setState({
      textStyle: commandState,
      position,
      showActionBar: selection.isCollapsed === false
    });
  }

  _textStyle(commandName) {
    document.execCommand(commandName);
  }

  render() {
    const { props } = this;
    const { current } = props.notes;
    const { textStyle } = this.state;

    const styleButtons = [
      {
        func: () => this._textStyle('bold'),
        value: <strong>graseto</strong>,
        isActive: textStyle.bold
      },
      {
        func: () => this._textStyle('italic'),
        value: <i>kursivo</i>,
        isActive: textStyle.italic
      },
      {
        func: () => this._textStyle('underline'),
        value: <u>substreko</u>,
        isActive: textStyle.underline
      }
    ];

    return (
      <div className="editor">
        <div
          contentEditable="true"
          id="editor"
          ref={this._editor}
          onSelect={ this._select }
          onScroll={props.scrollFn}>
          {current._id ? current.libel : null}
        </div>
        {this.state.showActionBar ? (
          <EditorActions
            items={styleButtons}
            flex={1}
            position={this.state.position}
          />
        ) : null}
      </div>
    );
  }
}

Editor.defaultProps = {
  match: null,
  history: null,
  _id: null
};

Editor.propTypes = {
  _id: PropTypes.string,
  match: PropTypes.object,
  history: PropTypes.object,
  findOne: PropTypes.func,
  insert: PropTypes.func,
  remove: PropTypes.func,
  removeCurrent: PropTypes.func,
  update: PropTypes.func,
  notes: PropTypes.shape({
    current: PropTypes.shape({
      _id: PropTypes.string,
      libel: PropTypes.string,
      _rev: PropTypes.string
    })
  }),
  books: PropTypes.shape({
    key: PropTypes.string
  })
};

export default Editor;
