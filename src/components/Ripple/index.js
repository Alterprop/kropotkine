import React from 'react';
import PropTypes from 'prop-types';
import ActionPanel from '../Page';

export default function Ripple(props) {
  console.log(props);
  const rippleFunc = (event) => {
    console.log(event);
  };

  return (
    <ActionPanel
      transitionName="rippleEffect"
      component="canvas"
      className="ripple"
    />
  );
}

export function withRipple(Component) {
  //top = event.pageX - this.offsetLeft;
  //  left = event.pageY - this.offsetTop;
  let X = null;
  let Y = null;
  const fireRipple = event => {
    console.log(event.target);
    X = event.pageX - window.offsetLeft;
    Y = event.pageY - window.offsetTop;
    console.log(X, Y);
  };

  return function(props) {
    console.log(props)
    const reClick = event => {
      props.func();
      fireRipple(event);
    };
    return <Component func={reClick} {...props}> <Ripple x={X} y={Y} /> </Component>
  };
}

Ripple.defaultProps = {
  color: 'rgba(0, 0, 0, 0.56)',
  func: null
};

Ripple.propTypes = {
  color: PropTypes.string,
  func: PropTypes.func
};
