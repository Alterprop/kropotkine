import React from 'react';
import PropTypes from 'prop-types';
import Icon from '../Icon';

export default function Empty(props) {
  return (
    <section className="empty">
      <Icon shape={props.icon} />
      <h3>{props.title}</h3>
      <span>{props.content}</span>
    </section>
  );
}

Empty.defaultProps = {
  icon: 'new',
  content: 'No se ha encontrado página',
  title: '0 resultados'
};

Empty.propTypes = {
  icon: PropTypes.string,
  content: PropTypes.string,
  title: PropTypes.string
};
