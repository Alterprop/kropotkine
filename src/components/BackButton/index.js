import React from 'react';
import PropTypes from 'prop-types';
import Button, { IconButton } from '../Button';
import { withRouter } from 'react-router-dom';

function BackButton(props) {
  const { backFunc, history, icon } = props;
  return icon ? (
    <IconButton func={backFunc || history.goBack} icon={icon} />
  ) : (
    <Button func={backFunc || history.goBack}>Atrás</Button>
  );
}

BackButton.defaultProps = {
  icon: 'back'
};

BackButton.propTypes = {
  history: PropTypes.shape({
    goBack: PropTypes.func
  }),
  backFunc: PropTypes.func.isRequired,
  icon: PropTypes.string
};

export default withRouter(BackButton);
