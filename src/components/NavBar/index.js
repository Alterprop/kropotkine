import React from 'react';
import { NavLink } from 'react-router-dom';
import PropTypes from 'prop-types';
import Icon from '../Icon';

export default function NavBar(props) {
  const paths = props.paths.map(
    (link, index) =>
      !link.external ? (
        <NavLink
          key={index}
          to={link.path}
          activeClassName="active-link"
          title={link.name}>
          {link.icon ? <Icon shape={link.icon} /> : link.alias || link.name  }
        </NavLink>
      ) : (
        <a key={index} title={link.name} href={link.path} target="_blank">
          {link.icon ? <Icon shape={link.icon} /> : null}
        </a>
      )
  );
  return (
    <nav role={props.role} className={props.type}>
      {paths}
    </nav>
  );
}

NavBar.defaultProps = {
  role: 'navigation',
  type: ''
};

NavBar.propTypes = {
  role: PropTypes.bool,
  paths: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  type: PropTypes.oneOf(['zoocial'])
};
