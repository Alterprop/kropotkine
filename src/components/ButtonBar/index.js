import React from 'react';
import PropTypes from 'prop-types';
import Button, { IconButton } from '../Button';

export default function ButtonBar(props) {

  const { height, flex, items } = props;
  const buttons = items.map(
    (item, index) =>
      item.icon ? (
        <IconButton key={index} {...item} />
      ) : (
        <Button
          key={index}
          type="menu-item"
          {...item}>
          {item.value}
        </Button>
      )
  );
  return (
    <div className={ `button-bar ${props.type ? props.type : '' }` } style={{ height }} flex={ flex }>
      {buttons}
    </div>
  );
}

ButtonBar.defaultProps = {
  items: {
    value: 'Sin Título',
    func: () => console.log('button')
  },
  height: 56,
  type: null
};

ButtonBar.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string,
      func: PropTypes.func
    })
  ),
  height: PropTypes.number,
  flex: PropTypes.number,
  type: PropTypes.string,
};
