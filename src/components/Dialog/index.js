import React, { Component, Children, cloneElement } from 'react';
import { createPortal } from 'react-dom';
import PropTypes from 'prop-types';
import ActionPanel from '../Page';
import Button from '../Button';
import Input from '../Input';

const initialState = { isOpen: false, promptValue: {} };

export default class Dialog extends Component {
  constructor(props) {
    super(props);
    this.state = initialState;
    this._open = this._open.bind(this);
    this._close = this._close.bind(this);
    this._confirm = this._confirm.bind(this);
    this._alert = this._alert.bind(this);
    this._prompt = this._prompt.bind(this);
    this._onPromptChange = this._onPromptChange.bind(this);
    this._savePromptValues = this._savePromptValues.bind(this);
  }

  _open() {
    this.setState({ isOpen: true });
  }

  _close() {
    this.setState({ isOpen: false }, () => this.setState(initialState));
  }

  _alert() {
    return (
      <section>
        <h3 className="dialog-title">{this.props.title}</h3>
        <p className="dialog-content">{this.props.text}</p>
        <footer>
          <Button func={this._close}>Aceptar</Button>
        </footer>
      </section>
    );
  }

  _confirm() {
    return (
      <section>
        <h3 className="dialog-title">{this.props.title}</h3>
        <p className="dialog-content">{this.props.text}</p>
        <footer>
          <Button
            func={event => {
              this.props.func(event.target);
              this._close();
            }}>
            Aceptar
          </Button>
          <Button func={this._close}>Cancelar</Button>
        </footer>
      </section>
    );
  }

  _onPromptChange(event) {
    const inputState = {};
    inputState[event.target.id] =
      event.target.type === 'checkbox'
        ? event.target.checked
        : event.target.value;
    this.setState(
      {
        promptValue: Object.assign({}, this.state.promptValue, inputState)
      },
      () => console.log(this.state.promptValue)
    );
  }

  _savePromptValues() {
    this.props.func(
      this.state.promptValue.promptKey,
      this.state.promptValue.promptState
    );
    this._close();
  }

  _prompt() {
    return (
      <section>
        <Input
          id="promptState"
          type="checkbox"
          name="libro-persistente"
          label={this.props.label}
          onChange={this._onPromptChange}
        />
        <h3 className="dialog-title">{this.props.title}</h3>
        <p className="dialog-content">{this.props.text}</p>
        <Input
          id="promptKey"
          name="libro-crudo"
          label="Nuevo libro crudo"
          onChange={this._onPromptChange}
        />
        <footer>
          <Button func={this._savePromptValues}>Aceptar</Button>
        </footer>
      </section>
    );
  }

  render() {
    const { children, transition } = this.props;

    const Trigger = Children.map(children, (child, index) => {
      if (index === 0) {
        return cloneElement(child, { onClick: this._open });
      }
    });

    const Content = Children.map(children, (child, index) => {
      if (index === 1) return child;
    });

    const Actions = Children.map(children, (child, index) => {
      if (index === 2) return child;
    });

    return (
      <span>
        {Trigger ? Trigger : <button onClick={this._open}>Abrir</button>}
        {this.state.isOpen
          ? createPortal(
            <div>
              <ActionPanel
                transitionName={transition}
                component="dialog"
                className="dialog"
                open={this.state.isOpen}>
                {Content &&
                  !this.props.confirm &&
                  !this.props.prompt &&
                  !this.props.alert ? (
                    <section className="dialog-content">
                      {Content}
                      <footer>
                        {Actions}
                        <button onClick={this._close}>Cerrar</button>
                      </footer>
                    </section>
                  ) : null}

                {this.props.confirm ? this._confirm() : null}

                {this.props.alert ? this._alert() : null}

                {this.props.prompt ? this._prompt() : null}
              </ActionPanel>

              <ActionPanel
                transitionName={transition}
                component="div"
                className="backdrop"
              />
            </div>,
            document.body
          )
          : null}
      </span>
    );
  }
}

Dialog.defaultProps = {
  title: '',
  text: '',
  label: '',
  confirm: false,
  prompt: false,
  alert: false,
  children: null,
  confirmFunc: null,
  changeFunc: null,
  func: null
};

Dialog.propTypes = {
  title: PropTypes.string,
  text: PropTypes.string,
  label: PropTypes.string,
  confirm: PropTypes.bool,
  prompt: PropTypes.bool,
  alert: PropTypes.bool,
  transition: PropTypes.string,
  children: PropTypes.element,
  confirmFunc: PropTypes.func,
  changeFunc: PropTypes.func,
  func: PropTypes.func
};
