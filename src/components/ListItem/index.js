import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import Floreal from '../../lib/floreal';
import Icon from '../Icon';

const capitalize = str => `${str.charAt(0).toUpperCase()}${str.slice(1)}`;
const formatDate = date => {
  const floreal = new Floreal(date);
  return `${floreal.day()} ${capitalize(floreal.monthName('es'))}`;
};

export default class ListItem extends Component {
  constructor(props) {
    super(props);
    this.onSwipeEnd = this._onSwipeEnd.bind(this);
    this.onSwipeListener = this._onSwipeListener.bind(this);
    this.state =  {};
  }

  render() {
    const { item } = this.props;
    return (
      <li className="list-item">
        <NavLink to={`/notes/${item._id}`} className="list-inner" style={{ left: this.state.left, position: 'relative' }}>
          <Icon
            shape="badge"
            fill={item.relevant ? '#0097A7' : 'rgba(0,0,0,0.16)'}
          />
          <span className="content">{item.libel}</span>
          <footer>
            <time>{formatDate(item.time)}</time>
          </footer>
        </NavLink>
      </li>
    );
  }

  _onSwipeEnd(event) {
    console.log('END', event);
  }
  _onSwipeListener(e) {
    if (e[0] > -100 && e[0] < 100) {
      this.setState({ left: e[0] });
    }
  }
}
// {item.habilidades.map((skill, index) => <span key={index}>{skill}</span>)}
ListItem.defaultProps = {
  item: {
    title: 'Sin Título'
  }
};

ListItem.propTypes = {
  item: PropTypes.shape({
    _id: PropTypes.string,
    title: PropTypes.string
  })
};
