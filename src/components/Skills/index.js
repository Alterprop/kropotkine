import React from 'react';
import PropTypes from 'prop-types';

export default function Skills(props) {
  const classes = `skills-list ${ props.show ? 'open' : '' }`;
  return (
    <ul className={ classes } >
      <h3>{props.title}</h3>
      {props.skills.map((skill, index) => <li key={index}>{skill}</li>)}
    </ul>
  );
}

Skills.defaultProps = {
  show: false,
  title: 'Skills'
};

Skills.propTypes = {
  skills: PropTypes.arrayOf(PropTypes.strings).isRequired,
  show: PropTypes.bool,
  title: PropTypes.string
};
