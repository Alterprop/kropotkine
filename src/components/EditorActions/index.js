import React from 'react';
import PropTypes from 'prop-types';
import ButtonBar from '../ButtonBar';

export default function EditorActions(props) {
  const { x, y } = props.position;
  return (
    <div className="editor-actions" style={{ top: y, left: x, zIndex: 1000000 }}>
      <ButtonBar {...props} />
    </div>
  );
}

EditorActions.defaultProps = {
  items: {
    value: 'Sin Título',
    func: () => console.log('button')
  },
  height: 56
};

EditorActions.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      value: PropTypes.string,
      func: PropTypes.func
    })
  ),
  height: PropTypes.number,
  flex: PropTypes.number
};
