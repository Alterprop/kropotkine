import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import Icon from '../Icon';
import Dialog from '../Dialog';

export default function Button(props) {

  const classes = `${props.type} ${props.isActive ? 'active' : '' }`;

  const Kbutton = (action) => (<button
    onClick={action ? props.func : null }
    className={classes}
    role={props.role ? 'button' : null}>
    {props.icon ? <Icon shape={props.icon} fill={props.iconColor} /> : null}
    {props.children}
  </button>);

  return props.link ? (
    <Link to={ props.link } className={ props.type }>
      {props.icon ? <Icon shape={props.icon} fill={props.iconColor} /> : null}
      { props.children }
    </Link>
  ) : (
    props.confirm || props.prompt || props.alert ?
      <Dialog {...props}>
        { Kbutton() }
      </Dialog>
      : Kbutton(true)
  );
}

export function IconButton(props) {
  return <Button {...props} type="iconico" />;
}

export function FabButton(props) {
  const { position } = props;
  return <Button {...props} type={ `fab ${ position || '' }` } />;
}

Button.defaultProps = {
  text: null,
  func: null,
  icon: null,
  iconColor: null,
  role: false,
  type: 'btn',
  children: null,
  link: null,
  position: null,
  isActive: null,
  confirm: null,
  prompt: null,
  alert: null
};

Button.propTypes = {
  text: PropTypes.string,
  func: PropTypes.func,
  icon: PropTypes.string,
  iconColor: PropTypes.string,
  role: PropTypes.bool,
  type: PropTypes.string,
  children: PropTypes.element,
  link: PropTypes.url,
  isActive: PropTypes.bool,
  position: PropTypes.string,
  confirm: PropTypes.bool,
  prompt: PropTypes.bool,
  alert: PropTypes.bool,
};

FabButton.propTypes = Button.propTypes;
