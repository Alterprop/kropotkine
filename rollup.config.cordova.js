import babel from 'rollup-plugin-babel';
import eslint from 'rollup-plugin-eslint';
import resolve from 'rollup-plugin-node-resolve';
import commonjs from 'rollup-plugin-commonjs';
import globals from 'rollup-plugin-node-globals';
import builtins from 'rollup-plugin-node-builtins';
import json from 'rollup-plugin-json';
import replacePlugin from 'rollup-plugin-replace';

export default {
  input: 'src/index.js',
  output: {
    format: 'iife',
    file: 'cordova/www/js/index.js'
  },
  plugins: [
    resolve({
      jsnext: true,
      main: true,
      browser: true,
      preferBuiltins: true
    }),
    json({ preferConst: true }),
    eslint(),
    babel({
      exclude: 'node_modules/**'
    }),
    replacePlugin({
      // Previene error React
      'process.env.NODE_ENV': JSON.stringify('production')
    }),
    commonjs({
      include: 'node_modules/**',
      namedExports: {
        'node_modules/react/index.js': [
          'Component',
          'PureComponent',
          'Children',
          'createElement',
          'cloneElement'
        ],
        'node_modules/react-dom/index.js': [
          'findDOMNode',
        ]
      }
    }),
    builtins(),
    globals()
  ]
};
