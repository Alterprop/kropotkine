Kropotkine
================
**Máquina de ideas** / **Free speech apparatus**

*Kropotkine* es una app de notas (móvil, web, escritorio), que permitirá el almacenamiento local y remoto de cuadernos de notas con diferentes claves de encriptación.

Ejecutar:

`npm run watch`

Browser:

`localhost:8080`
